import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/app/users/users.service';
import { compareSync } from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UsersService) {}

  async validateUser(email: string, password: string) {
    try {
      const user = await this.userService.findOneOrFail({ where: { email } });
      const isPasswordValid = compareSync(password, user.password);

      return isPasswordValid ? user : null;
    } catch (error) {
      return null;
    }
  }
}
