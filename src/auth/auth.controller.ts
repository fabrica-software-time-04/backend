import { Controller, Post, Req, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtService } from 'src/jwt/jwt.service';
import { ErrorTypeSwagger } from 'src/swagger/types/error-type.swagger';
import { LoginPostTypeSwagger } from 'src/swagger/types/login-post-type.swagger';
import { LocalAuthGuard } from './guards/local.guard';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private readonly jwtService: JwtService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiOperation({ summary: 'Login with credentials (email and password)' })
  @ApiResponse({
    status: 200,
    type: LoginPostTypeSwagger,
  })
  @ApiResponse({
    status: 400,
    type: ErrorTypeSwagger,
  })
  async login(@Req() req: any) {
    return this.jwtService.createAccessToken(req.user);
  }
}
