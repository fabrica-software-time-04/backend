export interface jwtPayload {
  user: {
    id: string;
    email: string;
  };
}
