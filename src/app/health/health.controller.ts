import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

@Controller('health')
@ApiTags('CI/CD')
export class HealthController {
  @Get()
  healthCheck() {
    return;
  }
}
