import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GenreEntity } from '../entities/genre.entity';
import { GenresService } from './genres.service';

@Module({
  imports: [TypeOrmModule.forFeature([GenreEntity])],
  providers: [GenresService],
  exports: [GenresService],
})
export class GenresModule {}
