import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { GenreEntity } from '../entities/genre.entity';

@Injectable()
export class GenresService {
  constructor(
    @InjectRepository(GenreEntity)
    private readonly genresRepository: Repository<GenreEntity>,
  ) {}

  async findOne(options: FindOneOptions<GenreEntity>) {
    return await this.genresRepository.findOne(options);
  }
}
