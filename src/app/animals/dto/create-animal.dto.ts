import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsNotEmpty, IsString } from 'class-validator';
export class CreateAnimalDto {
  @IsNotEmpty({ message: 'Nome é obrigatório' })
  @ApiProperty()
  name: string;

  @IsNotEmpty({ message: 'Data de nascimento é obrigatória' })
  @IsDateString({ message: 'Data de nascimento inválida' })
  @ApiProperty()
  birthdate: Date;

  @IsNotEmpty({ message: 'Peso do animal é obrigatório' })
  @ApiProperty()
  weight: number;

  @IsNotEmpty({ message: 'Gênero é obrigatório' })
  @IsString()
  @ApiProperty()
  genre: string;

  @IsNotEmpty({ message: 'Nome do pai é obrigatório' })
  @IsString()
  @ApiProperty()
  fatherName: string;

  @IsNotEmpty({ message: 'Nome da mãe é obrigatória' })
  @IsString()
  @ApiProperty()
  motherName: string;
}
