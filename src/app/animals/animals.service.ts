import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { AnimalEntity } from '../entities/animal.entity';
import { UsersService } from '../users/users.service';
import { CreateAnimalDto } from './dto/create-animal.dto';

@Injectable()
export class AnimalsService {
  constructor(
    @InjectRepository(AnimalEntity)
    private readonly animalRepository: Repository<AnimalEntity>,
    private readonly usersService: UsersService,
  ) {}

  async store(createAnimalDto: CreateAnimalDto, userId: string) {
    const user = await this.usersService.findOneOrFail({
      where: { id: userId },
    });
    return await this.animalRepository.save({ ...createAnimalDto, user });
  }

  async findOne(animalId: string, userId: string) {
    return await this.animalRepository.findOne({
      where: { id: animalId, user: { id: userId } },
    });
  }

  async findByUser(userId: string) {
    return await this.animalRepository.findBy({
      user: { id: userId },
    });
  }

  async findOneOrFail(options: FindOneOptions<AnimalEntity>) {
    try {
      return await this.animalRepository.findOneOrFail(options);
    } catch (error) {
      throw new NotFoundException(error.message);
    }
  }

  async destroy(animalId: string, userId: string) {
    await this.findOneOrFail({ where: { id: animalId, user: { id: userId } } });
    return await this.animalRepository.softDelete({
      id: animalId,
      user: { id: userId },
    });
  }
}
