import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  Req,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { jwtPayload } from 'src/auth/auth.types';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { ErrorTypeSwagger } from 'src/swagger/types/error-type.swagger';
import { UsersService } from '../users/users.service';
import { AnimalsService } from './animals.service';
import { CreateAnimalDto } from './dto/create-animal.dto';
import { AnimalGetPutTypeSwagger } from 'src/swagger/types/animal-get-put-type.swagger';
import { AnimalPostTypeSwagger } from 'src/swagger/types/animal-post-type.swagger';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
@Controller('animal')
@ApiTags('Animals')
@UseGuards(JwtAuthGuard)
export class AnimalsController {
  genresService: any;
  constructor(private readonly animalsService: AnimalsService) {}

  @Post('/')
  @ApiOperation({ summary: 'Create a new animal (animal registration)' })
  @ApiResponse({
    status: 200,
    type: AnimalPostTypeSwagger,
  })
  @ApiResponse({
    status: 400,
    type: ErrorTypeSwagger,
  })
  create(@Body() createAnimalDto: CreateAnimalDto, @Req() req: jwtPayload) {
    return this.animalsService.store(createAnimalDto, req.user.id);
  }

  @Get('/')
  @ApiOperation({ summary: 'Get animals' })
  @ApiResponse({
    status: 200,
    type: AnimalGetPutTypeSwagger,
  })
  @ApiResponse({
    status: 400,
    type: ErrorTypeSwagger,
  })
  find(@Req() req: jwtPayload) {
    return this.animalsService.findByUser(req.user.id);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get animal details (profile)' })
  @ApiResponse({
    status: 200,
    type: AnimalGetPutTypeSwagger,
  })
  @ApiResponse({
    status: 400,
    type: ErrorTypeSwagger,
  })
  findOne(@Param('id') animalId: string, @Req() req: jwtPayload) {
    return this.animalsService.findOne(animalId, req.user.id);
  }
}
