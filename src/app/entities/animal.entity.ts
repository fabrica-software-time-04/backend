import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserEntity } from './user.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'animals' })
export class AnimalEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id: string;

  @Column()
  name: string;

  @Column({ nullable: false })
  birthdate: Date;

  @Column({ nullable: false })
  weight: number;

  @Column({ nullable: false })
  genre: string;

  @Column({ nullable: false })
  fatherName: string;

  @Column({ nullable: false })
  motherName: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt: Date;

  @ManyToOne(() => UserEntity, (user) => user.animals)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;
}
