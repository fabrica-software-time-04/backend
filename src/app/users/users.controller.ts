import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { jwtPayload } from 'src/auth/auth.types';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { JwtService } from 'src/jwt/jwt.service';
import { ErrorTypeSwagger } from 'src/swagger/types/error-type.swagger';
import { UserGetPutTypeSwagger } from 'src/swagger/types/user-get-put-type.swagger';
import { UserPostTypeSwagger } from 'src/swagger/types/user-post-type.swagger';
import { GenresService } from '../genres/genres.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UsersService } from './users.service';

@Controller('user')
@ApiTags('User')
export class UsersController {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
    private readonly genresService: GenresService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create a new user (user registration)' })
  @ApiResponse({
    status: 200,
    type: UserPostTypeSwagger,
  })
  @ApiResponse({
    status: 400,
    type: ErrorTypeSwagger,
  })
  async store(@Body() body: CreateUserDto) {
    if (body.password !== body.confirmPassword) {
      throw new BadRequestException(['Senhas não conferem']);
    }

    const genre = await this.genresService.findOne({
      where: { id: body.genre },
    });

    if (!genre) {
      throw new BadRequestException(['Gênero inválido']);
    }

    const existingUser = await this.userService.findOne({
      where: { email: body.email },
    });

    if (existingUser) {
      throw new BadRequestException(['Já existe um usuário com esses dados']);
    }

    const storedUser = await this.userService.store(body);
    const { accessToken } = this.jwtService.createAccessToken(storedUser);

    delete storedUser.id;
    delete storedUser.password;
    delete storedUser.deletedAt;
    return {
      ...storedUser,
      genre: genre.genre,
      accessToken,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOperation({ summary: 'Get user details (profile)' })
  @ApiResponse({
    status: 200,
    type: UserGetPutTypeSwagger,
  })
  @ApiResponse({
    status: 400,
    type: ErrorTypeSwagger,
  })
  async show(@Req() req: jwtPayload) {
    const user = await this.userService.findOneOrFail({
      where: { id: req.user.id },
    });

    delete user.id;
    delete user.password;
    delete user.deletedAt;
    return user;
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  @ApiOperation({ summary: 'Update user data (profile)' })
  @ApiResponse({
    status: 200,
    type: UserGetPutTypeSwagger,
  })
  @ApiResponse({
    status: 400,
    type: ErrorTypeSwagger,
  })
  async update(@Req() req: jwtPayload, @Body() body: UpdateUserDto) {
    const updatedUser = await this.userService.update(req.user.id, body);

    delete updatedUser.id;
    delete updatedUser.password;
    delete updatedUser.deletedAt;

    return updatedUser;
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  @ApiOperation({ summary: 'Delete user data (remove application access)' })
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
  })
  @ApiResponse({
    status: 400,
    type: ErrorTypeSwagger,
  })
  async destroy(@Req() req: jwtPayload) {
    return await this.userService.destroy(req.user.id);
  }
}
