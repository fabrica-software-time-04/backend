import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsUUID,
  Matches,
} from 'class-validator';
import { regexHelper } from 'src/app/utils/regex.helper';
export class CreateUserDto {
  @IsNotEmpty({ message: 'Nome é obrigatório' })
  @ApiProperty()
  name: string;

  @IsNotEmpty({ message: 'Email é obrigatório' })
  @IsEmail({}, { message: 'Email inválido' })
  @ApiProperty()
  email: string;

  @IsOptional()
  @IsPhoneNumber('BR', { message: 'Numero de telefone inválido' })
  @ApiPropertyOptional()
  phone: string;

  @IsNotEmpty({ message: 'Data de nascimento é obrigatória' })
  @IsDateString({ message: 'Data de nascimento inválida' })
  @ApiProperty()
  birthdate: Date;

  @IsNotEmpty({ message: 'Gênero é obrigatório' })
  @IsUUID(4, { message: 'Gênero inválido' })
  @ApiProperty()
  genre: string;

  @IsNotEmpty({ message: 'Senha é obrigatória' })
  @Matches(regexHelper.password, {
    message:
      'A senha deve conter pelo menos 8 caracteres, uma letra maiúscula, uma letra minúscula, um número e um caractere especial',
  })
  @ApiProperty()
  password: string;

  @IsNotEmpty({ message: 'Confirmação da senha é obrigatória' })
  @Matches(regexHelper.password, {
    message:
      'A senha deve conter pelo menos 8 caracteres, uma letra maiúscula, uma letra minúscula, um número e um caractere especial',
  })
  @ApiProperty()
  confirmPassword: string;
}
