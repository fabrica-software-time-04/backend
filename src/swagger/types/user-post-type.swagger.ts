import { ApiProperty, OmitType } from '@nestjs/swagger';
import { UserEntity } from 'src/app/entities/user.entity';

export class UserPostTypeSwagger extends OmitType(UserEntity, [
  'id',
  'deletedAt',
  'password',
]) {
  @ApiProperty()
  accessToken: string;
}
