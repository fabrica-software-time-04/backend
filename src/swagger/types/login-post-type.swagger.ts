import { ApiProperty } from '@nestjs/swagger';

export class LoginPostTypeSwagger {
  @ApiProperty()
  accessToken: string;
}
