import { ApiProperty, OmitType } from '@nestjs/swagger';
import { AnimalEntity } from 'src/app/entities/Animal.entity';

export class AnimalPostTypeSwagger extends OmitType(AnimalEntity, [
  'id',
  'deletedAt',
]) {
  @ApiProperty()
  accessToken: string;
}
