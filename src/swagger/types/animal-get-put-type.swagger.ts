import { OmitType } from '@nestjs/swagger';
import { AnimalEntity } from 'src/app/entities/animal.entity';

export class AnimalGetPutTypeSwagger extends OmitType(AnimalEntity, [
  'id',
  'deletedAt',
]) {}
