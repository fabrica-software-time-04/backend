import { ApiProperty } from '@nestjs/swagger';

export class ErrorTypeSwagger {
  @ApiProperty()
  statusCode: number;

  @ApiProperty()
  message: string[];

  @ApiProperty()
  error: string;
}
