import { OmitType } from '@nestjs/swagger';
import { UserEntity } from 'src/app/entities/user.entity';

export class UserGetPutTypeSwagger extends OmitType(UserEntity, [
  'id',
  'password',
  'deletedAt',
]) {}
